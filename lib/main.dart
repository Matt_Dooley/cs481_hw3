import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  bool _isFavorited = false;
  int _favoriteCount = 1397;

  int currentImage = 0;
  int info = 0;

  @override
  Widget build(BuildContext context) {
    Widget titleSection = Container(
      padding: const EdgeInsets.all(32),
      child: Row(
        children: [
          Expanded(
            /*1*/
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                /*2*/
                Container(
                  padding: const EdgeInsets.only(bottom: 8),
                  child: Text(
                    'San Elijo State Beach',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                Text(
                  'Cardiff, CA',
                  style: TextStyle(
                    color: Colors.grey[500],
                  ),
                ),
              ],
            ),
          ),
          /*3*/
          _FavoriteWidget(),
        ],
      ),
    );

    Color color = Theme.of(context).primaryColor;

    Widget buttonSection = Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          _buildPreviousButtonColumn(),
          _buildInfoButtonColumn(),
          _buildPhoneButtonColumn(),
          _buildHoursButtonColumn(),
          _buildNextButtonColumn(),
        ],
      ),
    );

    Widget textSection0 = Container(
      padding: const EdgeInsets.all(32),
      child: Text(
        'Park Info: \n\n'
            'Located on the San Diego Coast, San Elijo offers\n'
            'swimming, surfing and picnicking. The narrow, bluff-\n'
            'backed stretch of sand has a nearby reef popular\n'
            'with snorklers and divers.',
        softWrap: true,
      ),
    );

    Widget textSection1 = Container(
      padding: const EdgeInsets.all(32),
      child: Text(
        'Phone Number: \n\n'
            '(760) 753-5091',
        softWrap: true,
      ),
    );

    Widget textSection2 = Container(
      padding: const EdgeInsets.all(32),
      child: Text(
        'Park Hours: \n\n'
            'Dawn to sunset.',
        softWrap: true,
      ),
    );

    return MaterialApp(
      title: 'CS481 HW3',
      home: Scaffold(
        appBar: AppBar(
          title: Text('CS481 HW3'),
        ),
        body: ListView(
          children: [
            Image.asset(
              'images/SanElijo$currentImage.jpg',
              width: 600,
              height: 240,
              fit: BoxFit.cover,
            ),
            titleSection,
            buttonSection,
            if(info == 0)
              textSection0,
            if(info == 1)
              textSection1,
            if(info == 2)
              textSection2,
          ],
        ),
      ),
    );
  }

  Column _buildPreviousButtonColumn() {
    return Column(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        IconButton(
          icon: Icon(Icons.navigate_before),
          color: Colors.blue[500],
          onPressed: _togglePrevious,
        ),
        Container(
          margin: const EdgeInsets.only(top: 8),
          child: Text(
            '   Previous   ',
            style: TextStyle(
              fontSize: 12,
              fontWeight: FontWeight.w400,
              color: Colors.blue[500],
            ),
          ),
        ),
      ],
    );
  }

  Column _buildNextButtonColumn() {
    return Column(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        IconButton(
          icon: Icon(Icons.navigate_next),
          color: Colors.blue[500],
          onPressed: _toggleNext,
        ),
        Container(
          margin: const EdgeInsets.only(top: 8),
          child: Text(
            '     Next     ',
            style: TextStyle(
              fontSize: 12,
              fontWeight: FontWeight.w400,
              color: Colors.blue[500],
            ),
          ),
        ),
      ],
    );
  }

  Column _buildInfoButtonColumn() {
    return Column(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        IconButton(
          icon: Icon(Icons.info_outline),
          color: Colors.blue[500],
          onPressed: _toggleInfo,
        ),
        Container(
          margin: const EdgeInsets.only(top: 8),
          child: Text(
            '  Park Info  ',
            style: TextStyle(
              fontSize: 12,
              fontWeight: FontWeight.w400,
              color: Colors.blue[500],
            ),
          ),
        ),
      ],
    );
  }

  Column _buildPhoneButtonColumn() {
    return Column(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        IconButton(
          icon: Icon(Icons.phone),
          color: Colors.blue[500],
          onPressed: _togglePhone,
        ),
        Container(
          margin: const EdgeInsets.only(top: 8),
          child: Text(
            ' Phone Number ',
            style: TextStyle(
              fontSize: 12,
              fontWeight: FontWeight.w400,
              color: Colors.blue[500],
            ),
          ),
        ),
      ],
    );
  }

  Column _buildHoursButtonColumn() {
    return Column(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        IconButton(
          icon: Icon(Icons.access_time),
          color: Colors.blue[500],
          onPressed: _toggleHours,
        ),
        Container(
          margin: const EdgeInsets.only(top: 8),
          child: Text(
            '  Park Hours  ',
            style: TextStyle(
              fontSize: 12,
              fontWeight: FontWeight.w400,
              color: Colors.blue[500],
            ),
          ),
        ),
      ],
    );
  }

  Row _FavoriteWidget() {
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: [
        Container(
          padding: EdgeInsets.all(0),
          child: IconButton(
            icon: (_isFavorited ? Icon(Icons.star) : Icon(Icons.star_border)),
            color: Colors.red[500],
            onPressed: _toggleFavorite,
          ),
        ),
        SizedBox(
          width: 32,
          child: Container(
            child: Text('$_favoriteCount'),
          ),
        ),
      ],
    );
  }

  void _toggleFavorite() {
    setState(() {
      if (_isFavorited) {
        _favoriteCount -= 1;
        _isFavorited = false;
      }
      else {
        _favoriteCount += 1;
        _isFavorited = true;
      }
    });
  }

  void _toggleNext() {
    setState(() {
      if (currentImage < 5) {
        currentImage += 1;
      }
    });
  }

  void _togglePrevious() {
    setState(() {
      if (currentImage > 0) {
        currentImage -= 1;
      }
    });
  }

  void _toggleInfo() {
    setState(() {
      info = 0;
    });
  }

  void _togglePhone() {
    setState(() {
      info = 1;
    });
  }

  void _toggleHours() {
    setState(() {
      info = 2;
    });
  }
}